package com.angular.springboot.backend.app.crud.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.angular.springboot.backend.app.crud.entities.Producto;

public interface IProductoDao extends JpaRepository<Producto, Long>{

}
