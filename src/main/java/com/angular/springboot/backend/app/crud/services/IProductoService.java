package com.angular.springboot.backend.app.crud.services;

import java.util.List;

import com.angular.springboot.backend.app.crud.entities.Producto;

public interface IProductoService {
	
	public List<Producto> getProducts();
	
	public Producto saveProduct(Producto product);
	
	public Producto findById(Long id);
	
	public void deleteById(Long id);

}