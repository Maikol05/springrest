package com.angular.springboot.backend.app.crud.entities;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.PrePersist;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.springframework.lang.Nullable;

@Entity
@Table(name="producto")
public class Producto implements Serializable{
	
	private static final long serialVersionUID = 1L;
    
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
	
	@NotBlank
	@Column(name="nombre", length = 50, unique = true)
	private String nombre;
	
	@Positive
	//@Column(name="precio", precision = 2)
	private double precio;
	
	@Column(name="tipo_producto", length = 50)
	private String tipoProducto;
	
	@Column(name="fecha_creado")
	@Temporal(TemporalType.DATE)
	private Date fecha;

	public Producto(String nombre, double precio,String tipoProducto) {
		this.nombre = nombre;
		this.precio = precio;
		this.tipoProducto = tipoProducto;
	}
	
	public Producto() {
		
	}
	
	@PrePersist
    public void prePersist() {
        this.fecha = new Date();
    }
	
	public String getTipoProducto() {
		return tipoProducto;
	}
	
	public void setTipoProducto(String tipoProducto) {
		this.tipoProducto = tipoProducto;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public double getPrecio() {
		return precio;
	}

	public void setPrecio(double precio) {
		this.precio = precio;
	}

	public Long getId() {
		return id;
	}
	
	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}
	
	public Date getFecha() {
		return fecha;
	}

}
