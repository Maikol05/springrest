package com.angular.springboot.backend.app.crud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootDataBackendCrud2Application {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootDataBackendCrud2Application.class, args);
	}

}
