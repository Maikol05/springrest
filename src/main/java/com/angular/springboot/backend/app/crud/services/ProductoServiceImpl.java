package com.angular.springboot.backend.app.crud.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.angular.springboot.backend.app.crud.dao.IProductoDao;
import com.angular.springboot.backend.app.crud.entities.Producto;

@Service
public class ProductoServiceImpl implements IProductoService{
	
	@Autowired
	IProductoDao productoDao;

	@Transactional(readOnly = true)
	@Override
	public List<Producto> getProducts() {
		
		return (List<Producto>) productoDao.findAll();
	}

	@Override
	public Producto saveProduct(Producto product) {
		return productoDao.save(product);		
	}

	@Transactional(readOnly = true)
	@Override
	public Producto findById(Long id) {
		return productoDao.findById(id).orElse(null);
	}

	@Override
	public void deleteById(Long id) {
		productoDao.deleteById(id);
	}

}
